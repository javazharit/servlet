package main.task;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class WebPageCreateAndHandlingServlet extends HttpServlet {
    private static final String PARAM_NAME = "field-p1";

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();

        HttpSession session = request.getSession();

        // setting session attribute as this should be re-used on each request
        String str = (String) session.getAttribute(PARAM_NAME);
        if (str == null) {
            str = " ";
        }

        // html form create
        pw.write("<html>");
        pw.write("<head><title>Servlet Testing...</title></head>");
        pw.write("<body>");
        pw.write("<form method=\"post\" action=\"/\">");
        pw.write("\n" +
                "        <table border=\"0\" cellspacing=\"5\" cellpadding=\"5\">\n" +
                "        <caption><h2>HTML Form</h2></caption>\n" +
                "        <tr>\n" +
                "        <td align=\"right\" valign=\"top\"><b>P1 field</b></td>\n" +
                "        <td><input type=\"text\" size=\"50\" name=\"field-p1\" value=\"" + str + "\"></td>\n" +
                "        </tr>\n" +
                "\n" +
                "        <tr>\n" +
                "        <td align=\"right\" colspan=\"2\">\n" +
                "        <input type=\"submit\" value=\"Submit\">\n" +
                "        </td>\n" +
                "        </tr>\n" +
                "\n" +
                "        </table>");
        pw.write("</form>");
        pw.write("</body>");
        pw.write("<html>");
        pw.close();
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html");

        HttpSession session = request.getSession();
        String answer = request.getParameter(PARAM_NAME);
        String doubleAnswer = answer + " " + answer;

        session.setAttribute(PARAM_NAME, doubleAnswer);
        response.sendRedirect("/");
    }

}
